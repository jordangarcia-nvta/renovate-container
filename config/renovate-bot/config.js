// Renovate Configuration Docs: https://renovatebot.com/docs/self-hosted-configuration
module.exports = {
  platform: "bitbucket",
  username: process.env.USERNAME,
  password: process.env.PASSWORD,
  gitAuthor: "Renovate Bot <bot@invitae.com>",
  onboarding: true,
  onboardingConfig: {
    extends: ["config:base"]
  },
  repositories: [process.env.REPOSITORIES],
  logFile: null,
  logFileLevel: "info"
};